// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
          // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
          // for form inputs)
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

          // Don't remove this line unless you know what you are doing. It stops the viewport
          // from snapping when text inputs are focused. Ionic handles this internally for
          // a much nicer keyboard experience.
          cordova.plugins.Keyboard.disableScroll(true);
      }

      StatusBar.hide();
      ionic.Platform.isFullScreen = true;
      ionic.Platform.fullScreen();
      if(window.StatusBar) {
          StatusBar.styleDefault();
      }
  });
})

.controller('SignatureCtrl', function($scope,SaveImg,$ionicLoading) {
    var canvas = document.getElementById('signatureCanvas');
    var signaturePad = new SignaturePad(canvas,{
        backgroundColor: 'transparent',
        penColor: '#000000', //black,
        minWidth:5,
        maxWidth:10
    });

    $scope.clearCanvas = function() {
        signaturePad.clear();
    }

    /**
     * ionic loading hide
     */
    $scope.loadingHide = function(){
        $ionicLoading.hide();
    }

    /**
    * set all pixels of the image to this color
    */
    function setCurrentColor(canvas, color) {
        var context = canvas.getContext('2d');
        context.save();
        context.fillStyle = color;
        context.globalCompositeOperation = 'source-in';
        context.fillRect(0,0,canvas.width, canvas.height);
        context.restore();
    }

    $scope.saveCanvas = function() {
        if (signaturePad.isEmpty()) {
            alert("Please provide signature first.");
        } else {
            $ionicLoading.show();

            // change drawline color
            var selectedColor = '#ffffff';//white
            signaturePad.penColor = selectedColor;
            setCurrentColor(canvas, selectedColor);

            var count = 0;
            var sigImg = signaturePad.toDataURL();

            //data save
            if(count == 0){
                var promise = SaveImg.saveImage(sigImg);
                promise.then(function (data)
                {
                    console.log(data);
                    count = 1;
                    // change drawline color
                    var selectedColor = '#000000';//black
                    signaturePad.penColor = selectedColor;
                    setCurrentColor(canvas, selectedColor);
                    $scope.clearCanvas();//clear canvas
                    $scope.loadingHide();//hide loading
                });
            }
        }
       // $scope.signature = sigImg;
    }
})

 .factory('SaveImg',function ($q,$http){

        var saveImage  = function(sigImg) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url:'http://dev.modernie.com/apps/dunhil/backend/',
                //url:'http://192.168.1.4:80/ionic-apps/landrover-signature-app/backend/',
                data: {
                  signatureImg: sigImg ,
                  appAction:'saveImage'
                }
            })
            .success(function (data, status, headers, config) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject('all search Jobs fail.');
            });
            return deferred.promise;
        };
        return {
            saveImage:saveImage
        };
    })