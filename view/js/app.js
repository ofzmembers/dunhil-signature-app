function getData(){
    $.ajax({
        url: 'http://dev.modernie.com/apps/dunhil/view/php/',
       // url: 'http://localhost/ionic-apps/landrover-signature-app/view/php/',
        data: {
            action: 'getImages'
        },
        error: function() {
            console.log("error");
        },
        success: function(result) {;
            var data = jQuery.parseJSON(result);
            if(data){
                var loop_count = (data.length- (data.length % 8 ))/8;
                var extra_count = data.length % 8;
                $(".clear-col").empty();
                var j = 0; // cell
                var divide_count = 1;
                var roundsCount = 0;
                var divideCount = 1;
                for(var i =0;i < data.length; i++){
                    j = j+1;
                    $('#col-' + j).append('<img class="signature_img" src="http://dev.modernie.com/apps/dunhil/backend/images/'+data[i].url+'">');
                    //$('#col-' + j).append('<img class="signature_img" src="http://localhost/ionic-apps/landrover-signature-app/backend/images/'+data[i].url+'">');

                    var imagesCount = i+1;
                    if(imagesCount<10){
                        // set 2 digits
                        $('#count').html('0'+imagesCount);
                    }else{
                        $('#count').html(imagesCount);
                    }

                    var img_width_cust = $(".cus-col").width()/divideCount;
                    console.log("signature_width" + img_width_cust);
                    $(".signature_img").css("width",img_width_cust + 'px');
                    if(j == 8){
                        j = 0;
                        roundsCount = roundsCount + 1;
                        if(roundsCount == divideCount * divideCount){
                            divideCount = divideCount + 1;
                        }
                    }
                }
            }
        },
        type: 'POST'
    });
}

$(document).ready(function(){
    setInterval(function() {
        getData();
    }, 3000);
})

function jsUpdateSize(){

    var width = window.innerWidth ||
        document.documentElement.clientWidth ||
        document.body.clientWidth;
    var height = window.innerHeight ||
        document.documentElement.clientHeight ||
        document.body.clientHeight;

    var col_height = height /3 ;
    console.log('col height = '+col_height);
    $('.cus-col').css('height',col_height);

    var logo_height = $("#logo").height() + 33;
    var margin_top_logo = (col_height - logo_height) /2 ;
    $('#logo').css('marginTop',margin_top_logo);

}

window.onload = jsUpdateSize;       // When the page first loads
window.onresize = jsUpdateSize;     // When the browser changes size
$(window).resize(jsUpdateSize);     // When the browser changes size