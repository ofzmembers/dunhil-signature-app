-- phpMyAdmin SQL Dump
-- version 3.5.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 06, 2016 at 11:17 AM
-- Server version: 5.5.42-37.1-log
-- PHP Version: 5.4.43

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `modernie_dunhil`
--

-- --------------------------------------------------------

--
-- Table structure for table `signature_detail`
--

CREATE TABLE IF NOT EXISTS `signature_detail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `img_url` text NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `signature_detail`
--

INSERT INTO `signature_detail` (`id`, `img_url`, `status`) VALUES
(1, '572caae7201f0.png', 1),
(2, '572caaf06e5ea.png', 1),
(3, '572cab0218354.png', 1),
(4, '572cab024137b.png', 1),
(5, '572cab0d5d6b3.png', 1),
(6, '572cab0eac1c0.png', 1),
(7, '572cab1bd30c6.png', 1),
(8, '572cab233ea41.png', 1),
(9, '572cab28a9970.png', 1),
(10, '572cab2d5525b.png', 1),
(11, '572cab350ee9e.png', 1),
(12, '572cab387a3c4.png', 1),
(13, '572cab43a8b7f.png', 1),
(14, '572cab45d02f0.png', 1),
(15, '572cab4aef91c.png', 1),
(16, '572cab53de2f8.png', 1),
(17, '572cab5863295.png', 1),
(18, '572cab5b126db.png', 1),
(19, '572cab62a29c7.png', 1),
(20, '572cab67c51d1.png', 1),
(21, '572cab6ce81f7.png', 1),
(22, '572cab760e272.png', 1),
(23, '572cab7629307.png', 1),
(24, '572cab7e22e0c.png', 1),
(25, '572cab8007dbd.png', 1),
(26, '572cab8925e57.png', 1),
(27, '572cab8edb273.png', 1),
(28, '572cab92a86b4.png', 1),
(29, '572cab95d341b.png', 1),
(30, '572cab9de5bfa.png', 1),
(31, '572caba3d8559.png', 1),
(32, '572caba7ed231.png', 1),
(33, '572cabb00af86.png', 1),
(34, '572cabbcc8e7b.png', 1),
(35, '572cabbee9da9.png', 1),
(36, '572cabc3c3c85.png', 1),
(37, '572cabc7290f0.png', 1),
(38, '572cabcb3142a.png', 1),
(39, '572cabd6a991c.png', 1),
(40, '572cabdb3539d.png', 1),
(41, '572cabe0f38a1.png', 1),
(42, '572cabe51b018.png', 1),
(43, '572cabedd1b0f.png', 1),
(44, '572cabf5bef50.png', 1),
(45, '572cac000b061.png', 1),
(46, '572cac030eb02.png', 1),
(47, '572cac0e48376.png', 1),
(48, '572cac39bc7bf.png', 1),
(49, '572cac59939a2.png', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
